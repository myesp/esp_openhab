#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <stdio.h>
#include <stdlib.h>

#define ADC1 0x68
#define ADC_CHANNEL1 0x98
#define ADC_CHANNEL2 0xB8
#define ADC_CHANNEL3 0xD8
#define ADC_CHANNEL4 0xF8


int main (int argc, char* argv[])
{
int var = wiringPiI2CSetup (ADC1);
int channel = 0;

if((var>0)&&(argc == 2)){
switch(atoi(argv[1])){
case 1: 
	channel= ADC_CHANNEL1;
	break;
case 2: 
	channel= ADC_CHANNEL2;
	break;
case 3: 	
	channel= ADC_CHANNEL3;
	break;
case 4: 
	channel= ADC_CHANNEL4;
	break;
   default: 
	printf("wrong adc channel config\n");
	return -1;
    }
}else{
 printf("wrong parameter config\n");
return -1;
}
int wiringPiI2CWriteReg16 (int fd, int reg, int data) ;

printf("channel: %d\n", atoi(argv[1]));
long inputvalue = wiringPiI2CReadReg16(var,channel);
printf("%lu", inputvalue);
printf("\n");
return inputvalue;
}
